-- create_views.sql
-- File ID: 24babc2a-fafb-11dd-96fe-000475e441b9

-- siste_aar: List all places from last year, DISTINCT ON name and every week
CREATE OR REPLACE VIEW siste_aar -- {{{
    AS SELECT * FROM (
        SELECT DISTINCT ON (
            name, date_trunc('week', date)
        ) *
        FROM logg
        WHERE date > now() + interval '1 year ago'
    ) AS s
    ORDER BY date; -- }}}

-- siste_halvaar: List all places from last six months, DISTINCT ON name and every week
CREATE OR REPLACE VIEW siste_halvaar -- {{{
    AS SELECT * FROM (
        SELECT DISTINCT ON (
            name, date_trunc('week', date)
        ) *
        FROM logg
        WHERE date > now() + interval '0.5 year ago'
    ) AS s
    ORDER BY date; -- }}}

-- siste_maaned: List all places from last month, DISTINCT ON name and every hour
CREATE OR REPLACE VIEW siste_maaned -- {{{
    AS SELECT * FROM (
        SELECT DISTINCT ON (
            name, date_trunc('hour', date)
        ) *
        FROM logg
        WHERE date > now() + interval '1 month ago'
    ) AS s
    ORDER BY date; -- }}}

-- siste_uke: List all places from last week, DISTINCT ON name and every hour
CREATE OR REPLACE VIEW siste_uke -- {{{
    AS SELECT * FROM (
        SELECT DISTINCT ON (
            name, date_trunc('hour', date)
        ) *
        FROM logg
        WHERE date > now()+interval '1 week ago'
    ) AS s
    ORDER BY date; -- }}}

-- siste_dogn: List all places from the last 24 hours, DISTINCT ON name and every minute
CREATE OR REPLACE VIEW siste_dogn -- {{{
    AS SELECT * FROM (
        SELECT DISTINCT ON (
            name, date_trunc('minute', date)
        ) *
        FROM logg
        WHERE date > now()+interval '1 day ago'
    ) AS s
    ORDER BY date; -- }}}

/*** Intervaller ***/

CREATE OR REPLACE VIEW minutt -- {{{
    AS SELECT * FROM (
        SELECT DISTINCT ON (
            date_trunc('minute', date)
        ) *
        FROM logg
    ) AS s
    ORDER BY date; -- }}}
CREATE OR REPLACE VIEW minuttname -- {{{
    AS SELECT * FROM (
        SELECT DISTINCT ON (
            date_trunc('minute', date),
            name
        ) *
        FROM logg
    ) AS s
    ORDER BY date; -- }}}
CREATE OR REPLACE VIEW hourname -- {{{
    AS SELECT * FROM (
        SELECT DISTINCT ON (
            date_trunc('hour', date),
            name
        ) *
        FROM logg
    ) AS s
    ORDER BY date; -- }}}

/*** Formater ***/

CREATE OR REPLACE VIEW closest AS -- {{{
    SELECT * FROM (
        SELECT DISTINCT ON (name) * FROM (
            SELECT * FROM LOGG
                ORDER BY dist
        ) AS b
        WHERE name IS NOT NULL
    ) AS a
        ORDER BY date; -- }}}

CREATE OR REPLACE VIEW gpx AS -- {{{
    SELECT '<trkpt lat="' || coor[0] || '" lon="' || coor[1] || '"> ' ||
        '<ele>' || ele || '</ele> ' ||
        '<time>' || date || '</time> ' ||
    '</trkpt>'
    AS gpx,
    date, coor, ele, name, dist, description
    FROM logg; -- }}}

CREATE OR REPLACE VIEW gpst AS -- {{{
    SELECT date, coor, ele, name, dist,
    '<tp> <time>' || date || 'Z' || '</time> <lat>' || coor[0] || '</lat> <lon>' || coor[1] || '</lon> </tp>'
    AS gpst
    FROM logg; -- }}}

-- ev: List events and the log
CREATE OR REPLACE VIEW ev AS -- {{{
    SELECT * FROM (
        SELECT     'gps' AS flag, date, coor, name || ' (' || dist || ')' AS name, ele::numeric(8,1), NULL AS descr
            FROM logg
        UNION ALL
        SELECT   'event' AS flag, date, coor, NULL, NULL, descr AS descr
            FROM events
        UNION ALL
        SELECT     'pic' AS flag, date, coor, filename, NULL, NULL
            FROM pictures
        UNION ALL
        SELECT    'film' AS flag, date, coor, filename, NULL, descr
            FROM film
        UNION ALL
        SELECT    'lyd' AS flag, date, coor, filename, NULL, descr
            FROM lyd
    ) AS u
    ORDER BY date; -- }}}

CREATE OR REPLACE VIEW media AS -- {{{
    SELECT 'film' AS what,
        date,
        filename,
        clname(coor) AS name,
        cldist(coor) AS dist,
        coor
        FROM film
    UNION ALL
    SELECT 'pic', date, filename, clname(coor), cldist(coor), coor
        FROM pictures
    UNION ALL
    SELECT 'lyd', date, filename, clname(coor), cldist(coor), coor
        FROM lyd
    ORDER BY date; -- }}}

-- wp: List all waypoints, sorted north → south, west → east
CREATE OR REPLACE VIEW wp AS -- {{{
    SELECT
        coor,
        name,
        type,
        cmt,
        ele,
        time
        FROM wayp
        ORDER BY coor[0] DESC, coor[1]; -- }}}
