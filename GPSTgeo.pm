package GPSTgeo;

#=======================================================================
# GPSTgeo.pm
# File ID: 6ac1a50e-fafa-11dd-a267-000475e441b9
#
# Character set: UTF-8
# ©opyleft 2002– Øyvind A. Holm <sunny@sunbase.org>
# License: GNU General Public License, see end of file for legal stuff.
#=======================================================================

use strict;
use warnings;

BEGIN {
    use Exporter ();
    our (@ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

    @ISA = qw(Exporter);
    @EXPORT = qw(&ddd_to_dms);
    %EXPORT_TAGS = ();
}
our @EXPORT_OK;

sub ddd_to_dms {
    # Convert floating-point degrees into D°M'S.S" (ISO-8859-1). 
    # Necessary for import into GPSman. Based on toDMS() from 
    # gpstrans-0.39 to ensure compatibility.
    # {{{
    my $ddd = shift;
    my $Neg = 0;
    my ($Hour, $Min, $Sec) =
       (    0,    0,    0);
    my $Retval = "";

    ($ddd =~ /^\-?(\d*)(\.\d+)?$/) || return(undef);
    length($ddd) || ($ddd = 0);

    if ($ddd < 0.0) {
        $ddd = 0 - $ddd;
        $Neg = 1;
    }
    $Hour = int($ddd);
    $ddd = ($ddd - $Hour) * 60.0;
    $Min = int($ddd);
    $Sec = ($ddd - $Min) * 60.0;

    if ($Sec > 59.5) {
        $Sec = 0.0;
        $Min += 1.0;
    }
    if ($Min > 59.5) {
        $Min = 0.0;
        $Hour += 1.0;
    }
    $Retval = sprintf("%s%.0f\xB0%02.0f'%04.1f\"",
                      $Neg
                        ? "-"
                        : "",
                      $Hour, $Min, $Sec);
    return $Retval;
    # }}}
} # ddd_to_dms()

1;
