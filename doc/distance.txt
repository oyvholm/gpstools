doc/distance.txt
File ID: f62e637a-fafa-11dd-990a-000475e441b9

Distances:

- 0.0002
  - N 70 E 23
    - N: 22 m
    - E: 8 m
    - NE: 23 m
    - Avg: 17.67 m

  - N 60 E 5
    - N: 22 m
    - E: 11 m
    - NE: 25 m
    - Avg: 19.33 m

  - N 0 E 0
    - N: 22 m
    - E: 22 m
    - NE: 31 m
    - Avg: 25.00 m

  - Total avg: 20.67 m

- 0.0003
  - N 60 E 5
    - N: 33 m
    - E: 17 m
    - NE: 37 m
    - Avg: 29 m

vim: set ts=2 sw=2 sts=2 :
